from addon import *


daily_quota_default = 150


def send_linkedin(browser, url, subject, message, file, min_delay, method = 2):
    browser.get(url)
    time.sleep(random.uniform(min_delay, min_delay + 3))
    
    #Customize message
    try:
        name = browser.find_element_by_xpath('//li[@class="inline t-24 t-black t-normal break-words"]').text
    except:
        name = ''
        pass
    
    subject = subject.replace('@subject', name)
    message = message.replace('@name', name)
        
    #If Message not locked
    if method == 1:
        elementID = browser.find_element_by_class_name('message-anywhere-button')
        
        if len(elementID.get_attribute('class').split()) > 1 \
            and elementID.get_property('href') != 'https://www.linkedin.com/premium/my-premium/':
            browser.find_element_by_class_name('pv-s-profile-actions--message').click()
            time.sleep(2)
            
            #Attach images  
            elementID = browser.find_element_by_xpath(
                '//input[@accept="image/*,.ai,.psd,.pdf,.doc,.docx,.csv,.zip,.rar,.ppt,.pptx,.pps,.ppsx,'+
                '.odt,.rtf,.xls,.xlsx,.txt,.pub,.html,.7z,.eml"]'
            )
            for f in file:
                elementID.send_keys(util.path_media + f)
                time.sleep(2)
                
            try:
                browser.find_element_by_xpath('//input[@name="subject"]').send_keys(subject)
            except:
                pass
            
            browser.find_element_by_xpath('//div[@role="textbox" and @aria-multiline="true"]').click()
            
            actions = ActionChains(browser)
            for part in message.split('\n'):
                actions.send_keys(part)
                actions.key_down(Keys.SHIFT).key_down(Keys.ENTER).key_up(Keys.SHIFT).key_up(Keys.ENTER)
                time.sleep(2)
                
            actions.perform()
            time.sleep(2)
            
            send_button = browser.find_element_by_class_name('msg-form__send-button')
            
            clickable = False
            time_wait = time.time()
            while not clickable and time.time() < time_wait + implicit_time:
                try:
                    cursor = send_button.value_of_css_property('cursor')
                    if cursor != 'not-allowed':
                        clickable = True
                    break
                except:
                    continue
                
            if clickable:
                send_button.click()
                
        time.sleep(2)
        
        #If Follow is required
#        browser.find_element_by_class_name('pv-s-profile-actions--follow').click()

        # Connect
        # checkSentButton = browser.find_element_by_class_name('ml1')
        # if 'artdeco-button--disabled' in checkSentButton.get_attribute('class').split():
        #     browser.find_element_by_class_name('artdeco-modal__dismiss').click()
        #     browser.find_element_by_class_name('pv-s-profile-actions--message').click()
        #     if True == check_exists_class('msg-form__subject'):
        #         browser.find_element_by_class_name('msg-form__subject').send_keys(subject)
        #     if True == check_exists_class('msg-form__contenteditable'):
        #         browser.find_element_by_class_name('msg-form__contenteditable').send_keys(message)
        #     if True == check_exists_class('msg-form__send-button'):
        #         browser.find_element_by_class_name('msg-form__send-button').click()
        # else:
        #     checkSentButton.click()
            
    # If Message is locked
    else:
        browser.find_element_by_xpath(
            '//button[@data-control-name="connect" and contains(@aria-label, "Connect with")]').click()
        time.sleep(2)
        
        try:
            browser.find_element_by_xpath('//button[contains(@aria-label, "Add a note")]').click()
        except:
            pass
        
        actions = ActionChains(browser)
        for part in message.split('\n'):
            actions.send_keys(part)
            actions.key_down(Keys.SHIFT).key_down(Keys.ENTER).key_up(Keys.SHIFT).key_up(Keys.ENTER)
            time.sleep(2)
            
        actions.perform()
        time.sleep(2)
        
        browser.find_element_by_class_name('ml1').click()
    
    time.sleep(2)
    ActionChains(browser).send_keys(Keys.ESCAPE).perform()
    
    return name


def remove_linkedin(browser, linkedin, min_delay):
    browser.get(linkedin)
    time.sleep(random.uniform(min_delay, min_delay + 3))

    browser.find_element_by_class_name('pv-s-profile-actions__overflow-toggle').click()
    time.sleep(2)
    
    browser.find_element_by_class_name('pv-s-profile-actions--disconnect').click()
    time.sleep(2)


def start_bot_linkedin(log, browser, run_succeed, df_message, df_data, df_notsent, func, min_delay):
    df_subject = df_message['SUBJECT']
    df_content = df_message['CONTENT']
    len_message = len(df_message)
    ii = 0
    try:
        df_file = df_message['FILE']
    except:
        df_file = pd.DataFrame()
        
    for idx, row in df_notsent.iterrows():
        df_data.loc[idx, 'NAME_MODIFIED'] = util.username
        df_data.loc[idx, 'DATE_MODIFIED'] = datetime.now()
        
        subject = df_subject[ii]
        message = df_content[ii]
        try:
            file = df_file[ii].split(', ')
        except:
            file = []
            pass
        
        ii += 1
        if ii == len_message: #reset to first content
            ii = 0
            
        try:
            url = row['LINKEDIN']
            status = row['STATUS']
            
            if status != 'sent' and func == 'send':
                name = send_linkedin(browser, url, subject, message, file, min_delay)
                df_data.loc[idx, 'NAME'] = name
                df_data.loc[idx, 'STATUS'] = 'sent'
                run_succeed += 1
                log += '[{}] {}: sent\n'.format(idx, url)
                
            elif status != 'removed' and func == 'remove':
                remove_linkedin(browser, url, min_delay)
                df_data.loc[idx, 'STATUS'] = 'removed'
                run_succeed += 1
                log += '[{}] {}: removed\n'.format(idx, url)
                
            #Sleep to make sure everything loads
            time.sleep(random.uniform(min_delay + 3, min_delay + 7))
            
        except Exception as e:
            df_data.loc[idx, 'STATUS'] = e
            log += '[{}] {}: error\n'.format(idx, url)
            pass
        
    browser.quit()
    
    return log, run_succeed, df_data


def run_linkedin_message(username, password, filename = None, num_run = daily_quota_default, 
        daily_quota = daily_quota_default, ignore_error = False, min_delay = min_delay_default, method = 2, func = 'send'):
    service = 'linkedin'
    run_succeed = 0
    
    log = init_log()
    path, filename = get_path_from_filename(filename)
    
    log, df_message, df_data, df_notsent, \
        df_limit, daily_sent = read_data_message(log, username, path, service, num_run, ignore_error, daily_quota)
        
    log = print_log(log, 'Login to Linkedin.')
    browser = init_browser()
    log_temp = login_linkedin(browser, username, password)
    
    if log_temp != '':
        log = print_log(log, log_temp)
        browser.quit()
        return log
    
    else:
        if func == 'send':
            log = print_log(log, 'Linkedin Bot: START sending..\n')
        elif func == 'remove':
            log = print_log(log, 'Linkedin Bot: START removing..\n')
            
        log, run_succeed, df_data = start_bot_linkedin(log, browser, run_succeed,
                                        df_message, df_data, df_notsent, func, min_delay)
        
        #Switch content
        df_message = df_message.apply(np.roll, shift = 1)
        
        #Update data log
        log += update_log_limit(df_limit, username, run_succeed, daily_sent)
        
        #Update results
        log += export_data_message(path, df_message, df_data)
        
        log = print_log(log, 'Linkedin Bot: DONE.')
    
    return log




