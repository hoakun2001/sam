from addon import *


def run_company_scrape_info(filename=None, column_name=None, keyword='market cap | valuation billion',
    classname='div', att={'class': 'LGOjhe'}, num_export=50, min_delay=min_delay_default):
    log, data, output_name = import_file(filename)

    if column_name is None:
        col_index = 0
        column_name = data.columns[col_index]
    else:
        col_index = data.columns.get_loc(column_name)

    data_prune = data.dropna(subset=[column_name])
    data_prune = data_prune.drop_duplicates(subset=[column_name])
    if keyword in data_prune.columns:
        data_exist = data_prune.dropna(subset=[keyword])
        data_prune = data_prune.drop(data_exist.index)
    log = print_log(log, 'Data processing: %s' % len(data_prune))

    data[keyword] = ''
    first_result = True
    log = print_log(log, 'Keyword: %s' % keyword)
    log = print_log(log, 'Scraping Bot: START scraping..\n')

    for idx, row in data_prune.iterrows():
        t_start = time.time()
        print(idx)

        search_term = str(row[column_name]) + ' ' + keyword
        html = fetch_results(search_term)
        soup = BeautifulSoup(html, 'html.parser')
        time.sleep(min_delay)

        try:
            if classname is None or att is None or keyword is None:
                result = parse_results(html)
                res = result.loc[0, 'link'].split('www.')[-1][:-1]
                data.loc[idx, keyword] = res

            else:
                result = soup.find(classname, attrs=att)
                res = result.text
                data.loc[idx, keyword] = res

            if first_result is True:
                log = print_log(log, '%s: %s\n' % (row[column_name], res))
                log = print_log(log, 'Elapsed time: %.2f seconds.\n' % (time.time() - t_start))
                first_result = False

            if idx != len(data_prune)-1:
                if (idx + 1) % num_export == 0:
                    [os.remove(os.path.join(util.path_output, f)) for f in os.listdir(util.path_output)
                        if f.find(output_name) != -1]
                    path_user = util.path_output + output_name + '__DONE_%s.xlsx' % (idx + 1)
                    log = export_file(data, path_user, log)

            else:
                # remove all temp output
                log = print_log(log, 'Remove all temporary outputs.')
                [os.remove(os.path.join(util.path_output, f)) for f in os.listdir(util.path_output)
                    if f.find(output_name) != -1]

                path_user = util.path_output + output_name + '__DONE.xlsx'
                log = export_file(data, path_user, log)

        except Exception as e:
            print(e)
            pass

    log = print_log(log, 'Scraping Bot: DONE.')

    return log


def run_company_scrape_linkedin(filename=None, column_name=None,
                                keyword='site:linkedin.com/in/ & linkedin ceo | founder',
                                num_result=3, num_export=50, min_delay=min_delay_default):
    log, data, output_name = import_file(filename)

    if column_name is None:
        col_index = 0
    else:
        col_index = data.columns.get_loc(column_name)

    data_total = pd.DataFrame(columns=['Company name', 'link', 'title', 'info'])
    log = print_log(log, 'Keyword: %s' % keyword)
    log = print_log(log, 'Scraping Bot: START scraping..\n')

    for idx, row in data.iterrows():
        print(idx)
        try:
            search_term = str(row.iloc[col_index]) + ' ' + keyword
            search_term = convert_ascii(search_term)

            results = scrape_google(search_term, num_result)
            results = results[[i.find('linkedin.com/in/') != -1 for i in results['link']]].reset_index(drop=True)
            results['Company name'] = row.iloc[col_index]
            data_total = pd.concat([data_total, results])
            time.sleep(min_delay)

            if idx == 0:
                log = print_log(log, '%s\n' % results.loc[0])

            elif idx != len(data)-1:
                if (idx + 1) % num_export == 0:
                    [os.remove(os.path.join(util.path_output, f)) for f in os.listdir(util.path_output)
                        if f.find(output_name) != -1]
                    path_user = util.path_output + output_name + '__DONE_%s.xlsx' % (idx + 1)
                    log = export_file(data_total, path_user, log)

            else:
                # remove all temp output
                log = print_log(log, 'Remove all temporary outputs.')
                [os.remove(os.path.join(util.path_output, f)) for f in os.listdir(util.path_output)
                    if f.find(output_name) != -1]

                path_user = util.path_output + output_name + '__DONE.xlsx'
                log = export_file(data_total, path_user, log)

        except Exception as e:
            print(e)
            pass

    log = print_log(log, 'Scraping Bot: DONE.')

    return log


def get_linkedin_info(browser, linkedin, min_delay):
    res = pd.Series()

    browser.get(linkedin)
    time.sleep(min_delay)

    try:
        element_list = browser.find_elements_by_xpath('//span[@class="visually-hidden"]')
        browser = remove_element(browser, element_list)
        element_list = browser.find_elements_by_xpath('//span[@class="dist-value"]')
        browser = remove_element(browser, element_list)

        profile = browser.find_element_by_xpath('//div[@class="ph5 pb5"]').text
        profile = profile.replace('Connect\n', '').replace('Message\n', '').replace('More…\n', '')
        list_profile = profile.split('\n')

        fullname = list_profile[0].split(',')[0]
        res['First name'] = fullname.split(' ')[0]
        res['Last name'] = fullname.split(' ')[-1]

        job = list_profile[1]
        res['Job title'] = job

        country = list_profile[2].replace(' Contact info', '')
        res['Country/Region'] = country

    except:
        res['First name'] = ''
        res['Last name'] = ''
        res['Job title'] = ''
        res['Country/Region'] = ''

    try:
        company_name = list_profile[3]
        res['Company name'] = company_name

        num_connections = [x for x in list_profile if 'connection' in x][0]
        res['Connection'] = str(num_connections)

    except:
        res['Company name'] = ''
        res['Connection'] = ''

    try:
        element_list = browser.find_elements_by_xpath('//a[@data-control-name="browsemap_profile"]')
        list_href = [i.get_attribute('href').split('/in/')[-1] for i in element_list]
        res['Also viewed'] = ', '.join(list_href)

    except:
        res['Also viewed'] = ''

    browser.get(linkedin + 'detail/contact-info/')
    time.sleep(5)

    try:
        twitter = browser.find_element_by_xpath('//a[contains(@href, "twitter.com/")]').text
        twitter = twitter.replace('twitter.com/', '')

    except:
        twitter = ''
    res['Twitter username'] = twitter

    try:
        email = browser.find_element_by_xpath('//a[contains(@href, "mailto")]').text

    except:
        email = ''
    res['Email'] = email

    return res


def run_linkedin_crawl_info(username, password, filename=None, headless=True, num_export=50, min_delay=min_delay_default):
    import warnings
    warnings.filterwarnings('ignore')

    log, data, output_name = import_file(filename)
    data_temp = pd.DataFrame(
        columns = ['Company name', 'LinkedIn', 'First name', 'Last name', 'Job title',
                               'Country/Region', 'Connection', 'Email', 'Twitter username',
                               'Also viewed', 'Website URL', 'Membership notes']
        )
    data = pd.merge(data, data_temp, how='left')

    data_prune = data.dropna(subset=['LinkedIn'])
    data_prune = data_prune.drop_duplicates(subset=['LinkedIn'])
    data_exist = data_prune.dropna(subset=['First name'])
    data_prune = data_prune.drop(data_exist.index)
    log = print_log(log, 'Data processing: %s' % len(data_prune))

    log = print_log(log, 'Login to LinkedIn.')
    browser = init_browser(headless=headless)
    log_temp = login_linkedin(browser, username, password)

    if log_temp != '':
        log = print_log(log, log_temp)
        browser.quit()
        return log

    else:
        first_result = True
        log = print_log(log, 'LinkedIn Bot: START crawling..\n')

        for idx, row in data_prune.iterrows():
            t_start = time.time()
            print(idx)
            try:
                linkedin = row['LinkedIn']
                # Preprocessing url
                linkedin = 'https://www.linkedin.com' + linkedin.split('linkedin.com')[1]
                linkedin = linkedin.split('?')[0]
                linkedin = linkedin.replace('%2522', '')
                if linkedin[-1] != '/':
                    linkedin += '/'

                res = get_linkedin_info(browser, linkedin, min_delay)

                if not pd.isnull(row['Company name']):
                    company_name = row['Company name']
                else:
                    company_name = res['Company name']

                if not pd.isnull(row['Website URL']):
                    website_url = row['Website URL']
                    website_url = website_url.replace('www.', '').replace('/', '')
                else:
                    website_url = ''

                if not pd.isnull(row['Membership notes']):
                    membership_note = row['Membership notes']
                else:
                    membership_note = ''

                data.loc[idx, 'Company name'] = company_name
                data.loc[idx, 'LinkedIn'] = linkedin
                data.loc[idx, 'First name'] = res['First name']
                data.loc[idx, 'Last name'] = res['Last name']
                data.loc[idx, 'Job title'] = res['Job title']
                data.loc[idx, 'Country/Region'] = res['Country/Region']
                data.loc[idx, 'Connection'] = res['Connection']
                data.loc[idx, 'Email'] = res['Email']
                data.loc[idx, 'Twitter username'] = res['Twitter username']
                data.loc[idx, 'Also viewed'] = res['Also viewed']
                data.loc[idx, 'Website URL'] = website_url
                data.loc[idx, 'Membership notes'] = membership_note

                if first_result is True and res['First name'] != '':
                    log = print_log(log, '%s\n' % data.loc[idx])
                    log = print_log(log, 'Elapsed time: %.2f seconds.\n' % (time.time() - t_start))
                    first_result = False

                if idx != len(data_prune)-1:
                    if (idx + 1) % num_export == 0:
                        [os.remove(os.path.join(util.path_output, f)) for f in os.listdir(util.path_output)
                            if f.find(output_name) != -1]
                        path_user = util.path_output + output_name + '__DONE_%s.xlsx' % (idx + 1)
                        log = export_file(data, path_user, log)

                else:
                    # remove all temp output
                    log = print_log(log, 'Remove all temporary outputs.')
                    [os.remove(os.path.join(util.path_output, f)) for f in os.listdir(util.path_output)
                        if f.find(output_name) != -1]

                    path_user = util.path_output + output_name + '__DONE.xlsx'
                    log = export_file(data, path_user, log)

            except Exception as e:
                print(e)
                pass

        browser.quit()

        log = print_log(log, 'LinkedIn Bot: DONE.')

        return log


def run_linkedin_invitation_withdraw(username, password, page=10, headless=True, min_delay=min_delay_default):
    log = init_log()

    log = print_log(log, 'Login to LinkedIn.')
    browser = init_browser(headless=headless)
    log_temp = login_linkedin(browser, username, password)

    if log_temp != '':
        log = print_log(log, log_temp)
        browser.quit()
        return log

    else:
        sent_link = 'https://www.linkedin.com/mynetwork/invitation-manager/sent/?invitationType=&page=%s' % page
        browser.get(sent_link)
        time.sleep(min_delay)

        # Scroll to end of page
        browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(min_delay)

        log = print_log(log, 'LinkedIn Bot: START Removing..\n')

        element_list = browser.find_elements_by_xpath('//div[contains(@class, "display-flex ph2 pt1")]')
        for idx, elementID in enumerate(element_list):
            try:
                info_list = elementID.text.split('\n')
                time_invite = info_list[-2]

                if time_invite == '3 weeks ago' or time_invite == '4 weeks ago' or time_invite == '1 month ago' or time_invite == '2 months ago':
                    elementID.find_element_by_xpath('.//button[@data-control-name="withdraw_single"]').click()
                    time.sleep(1)
                    popup = browser.switch_to.active_element
                    popup.find_element_by_xpath('//button[@class="artdeco-modal__confirm-dialog-btn ' +
                        'artdeco-button artdeco-button--2 artdeco-button--primary ember-view"]').click()

                    name = info_list[1]
                    log = print_log(log, '[%s] %s: %s' % (idx, name, time_invite))

                time.sleep(5)

            except:
                pass

        browser.quit()

        log = print_log(log, '\nLinkedIn Bot: DONE.')

    return log




