from addon import *



daily_quota_default = 10


def import_file(filename):
    log = ''
    log = print_log(log, '\n========== %s __ %s ==========\n' % (
        sys._getframe(1).f_code.co_name, datetime.now().strftime('%d-%b-%y %H:%M:%S').upper())
    )
    
    path, filename = get_path_from_filename(filename)    
    data = pd.read_excel(path)
    output_name = filename.split('.xlsx')[0]
    
    log = print_log(log, 'Input: %s' % path)
    log = print_log(log, 'Total data: %s' % len(data))
    
    return log, data, output_name


def export_file(data, path, log):            
    with pd.ExcelWriter(path) as writer:
        data.to_excel(writer, index = False)

    log = print_log(log, 'Output: %s\n' % path)
    
    return log


def login_linkedin(browser, username, password):
    log = ''
    url = 'https://www.linkedin.com/uas/login'
    
    browser.get(url)
    time.sleep(5)
    
    try:
        elementID = browser.find_element_by_id('username')
        elementID.send_keys(username)
        
        elementID = browser.find_element_by_id('password')
        elementID.send_keys(password)
        
        elementID.submit()
        
    except:
        browser.quit()
        return 'Login failed!'
        
    time.sleep(min_delay_default)
    
    #Minimizing Message Popup
    try:
        elementID = browser.find_element_by_xpath(
            '//section[@class="msg-overlay-bubble-header__controls display-flex"]')
        chevron_icon = elementID.find_elements_by_css_selector('li-icon')[-1].get_attribute('type')
        if chevron_icon == 'chevron-down-icon':
            elementID.find_elements_by_css_selector('button')[-1].click()
            
    except:
        pass
        
    return log


def login_facebook(browser, username, password):
    log = ''
    url = 'https://www.facebook.com/login'
    
    browser.get(url)
    time.sleep(5)
    
    try:
        elementID = browser.find_element_by_id('email')
        elementID.send_keys(username)
        
        elementID = browser.find_element_by_id('pass')
        elementID.send_keys(password)
        
        elementID.submit()
        
    except:
        browser.quit()
        return 'Login failed!'
        
    time.sleep(min_delay_default)
        
    return log


def get_path_from_filename(filename):
    if filename is None:
        path = lib_sys.get_filepath('xlsx')
        filename = os.path.basename(path)
        
        if len(path) == 0:
            return 'No file selected.'
    else:
        path = util.path_input + filename
    
    return path, filename


def check_log_limit(log, username, service, num_run, daily_quota):
    df_limit = pd.read_csv('./addon/addon_config/limit_log.csv')
    df_log = df_limit[df_limit['SERVICE'] == service]
    
    try:
        df_templog = df_log[df_log['USERNAME'] == username].iloc[0]
        daily_usage = df_templog['DAILY_USAGE']
        
        if daily_quota > 0:
            df_limit.loc[df_limit['USERNAME'] == username, 'DAILY_QUOTA'] = daily_quota
        else:
            daily_quota = df_templog['DAILY_QUOTA']
            
        last_modified_day = df_templog['DATE_MODIFIED']
        last_modified_day = datetime.strptime(last_modified_day, '%Y-%m-%d').date()
    except:
        if username not in df_log['USERNAME'].values:
            df_limit = df_limit.append(
                pd.DataFrame([{
                    'SERVICE': service,
                    'USERNAME': username,
                    'DATE_MODIFIED': datetime.today().date(),
                    'DAILY_USAGE': 0,
                    'DAILY_QUOTA': daily_quota_default,
                }]),
                ignore_index = True, sort = False
            )
        daily_usage = 0
        daily_quota = daily_quota_default
        last_modified_day = datetime.today().date()
        
    #Check limit message and connection per day
    if datetime.today().date() - last_modified_day >= timedelta(days = 1):
        daily_usage = 0
    log = print_log(log, 'Daily usage: %s __ Daily quota: %s' % (daily_usage, daily_quota))
    
    len_limit = daily_quota - daily_usage
    if num_run != 0:
        len_limit = min(len_limit, num_run)
        
    if len_limit <= 0:
        log = print_log(log, 'Account `%s` has reached the quota limit!' % username)
        return log
            
    return log, df_limit, daily_usage, len_limit


def update_log_limit(df_limit, username, run_succeed, daily_usage):
    log = ''
    
    try:
        df_limit.loc[df_limit['USERNAME'] == username, 'DAILY_USAGE'] = run_succeed + daily_usage
        df_limit.loc[df_limit['USERNAME'] == username, 'DATE_MODIFIED'] = datetime.today().date()
        df_limit.drop_duplicates(inplace = True, ignore_index = True)
        df_limit.to_csv('./addon/addon_config/limit_log.csv', index = False)
        
        log = print_log(log, '\nUpdate limit_log. Daily usage: %s' % (run_succeed + daily_usage))
        
    except:
        log = print_log(log, '\nUpdate limit_log failed! Daily usage: %s' % (run_succeed + daily_usage))
        pass
    
    return log


def read_data_message(log, username, path, service, num_run, ignore_error, daily_quota):    
    df_message = pd.read_excel(path, 'MESSAGE')
    df_data = pd.read_excel(path, 'DATA')
    
    log = print_log(log, 'Activating account: %s' % username)
    
    #Get latest data log
    log = print_log(log, 'Input: %s' % path)
    log, df_limit, daily_usage, len_limit = check_log_limit(log, username, service, num_run, daily_quota)
    
    #Get data not sent
    if ignore_error is False:
        df_notsent = df_data[pd.isnull(df_data['STATUS'])].head(len_limit)
    else:
        df_notsent = df_data[df_data['STATUS'] != 'sent'].head(len_limit)
        
    log = print_log(log, 'Total data: %s' % len(df_notsent))
    
    return log, df_message, df_data, df_notsent, df_limit, daily_usage


def export_data_message(path, df_message, df_data):
    log = ''
       
    path_user = path.split('.xlsx')[0] + '__DONE.xlsx'
    with pd.ExcelWriter(path_user) as writer:
        df_message.to_excel(writer, sheet_name = 'MESSAGE', index = False)
        df_data.to_excel(writer, sheet_name = 'DATA', index = False)

    os.remove(path)
    log = print_log(log, 'Output: %s\n' % path_user)
    
    return log
    
    
    
    
    
    
    
    
    